const debug = require('debug');
const {DefaultKnexConfig} = require('./config/db-config');
const {env} = require('./config');

/* Debug Logs */
const logKnexConfigVerbose = debug('gitter-archive:knex:config:verbose');

const TestConfig = DefaultKnexConfig.compose({
	deepProps: {
		connection: {
			database: env.PG_TEST_DATABASE || 'test'
		}
	}
});
logKnexConfigVerbose('Test Environment Configuration\n%o', TestConfig());

const DevConfig = DefaultKnexConfig.compose({
	deepProps: {
		connection: {
			database: env.PG_TEST_DATABASE || 'test'
		}
	}
});
logKnexConfigVerbose('Development Environment Configuration\n%o', DevConfig());

const ProdConfig = DefaultKnexConfig.compose({
	deepProps: {
		connection: {
			database: env.PG_TEST_DATABASE || 'test'
		}
	}
});
logKnexConfigVerbose('Production Environment Configuration\n%o', ProdConfig());

module.exports = {
	development: DevConfig(),
	test: TestConfig(),
	production: ProdConfig()
};
