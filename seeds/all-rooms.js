const appRoot = require('app-root-path');
const debug = require('debug');
const _ = require('lodash/fp');

// eslint-disable-next-line no-unused-vars
const logRooms = debug('gitter-archive:rooms');
const logRoomsVerbose = debug('gitter-archive:rooms:verbose');

const roomsTableName = 'Rooms';
const errorsTableName = 'Archive Errors';
const messagesTableName = 'Messages';

const {srcPath, appConfig} = require(`${appRoot}/config/app-config`);
const {getAllRooms} = require(`${appRoot}/${srcPath}/gitter`);

const groups = appConfig.get('groups');
logRoomsVerbose('Groups:\n%o', groups);

const {enabled: enabledGroups} = groups;
logRoomsVerbose('Enabled Groups:\n%o', enabledGroups);

const fccGroup = _.find({groupUri: 'FreeCodeCamp'}, enabledGroups);
logRooms('FCC Group: %o', fccGroup);

const {groupId: fccGroupId} = fccGroup;
logRooms('FCC Group Id: %o', fccGroupId);

const rooms = appConfig.get('rooms');
logRoomsVerbose('Rooms:\n%o', rooms);

const noArchiveList = rooms.noArchiveList;
logRoomsVerbose('No Archive Rooms:\n%o', noArchiveList);

const archiveNoDeleteList = rooms.archiveNoDeleteList;
logRoomsVerbose('Archive No Delete Rooms:\n%o', archiveNoDeleteList);

exports.seed = function (knex, Promise) {
	const promises = [];

	// fetch all rooms from gitter
	const batchPromise = getAllRooms({groupId: fccGroupId})
		.then(({rooms}) => _.map(room => {
			const isArchive = !noArchiveList.includes(room.uri);
			const isDeprecate = !archiveNoDeleteList.includes(room.uri);

			return {
				/* eslint-disable camelcase */
				room_id: room.id,
				room_uri: room.uri,
				group_id: room.groupId,
				user_count: room.userCount,
				is_archive: isArchive,
				is_deprecate: isDeprecate,
				/* eslint-enable camelcase */
			};
		}, rooms))
		// delete all entries in "Rooms" table
		.then(_.tap(() => knex(roomsTableName).del()))
		.then(rows => {
			logRooms('Seed Rooms');
			const batchPromise = knex.batchInsert(roomsTableName, rows);
			return batchPromise;
		});

	promises.push(
		batchPromise,

		// delete all entries in "errors" table
		knex(errorsTableName).del(),

		// delete all entries in "Messages" table
		knex(messagesTableName).del()
	);

	return Promise.all(promises);
};
