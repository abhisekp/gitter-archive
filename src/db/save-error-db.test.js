import test from 'ava';
import appRoot from 'app-root-path';
import {SaveError} from './save-error-db';
import {DbClientFactory} from './db-client-factory';
import {DbUtil} from './db-util';

const {currDbEnv} = require(`${appRoot}/config/db-config`);

const knexConfig = require(`${appRoot}/knexfile`)[currDbEnv];

const errorSaver = DbClientFactory.compose(DbUtil, SaveError).create(knexConfig);

const errorObj = {
	skip: 0,
	beforeId: '1234567890',
	roomId: '9876543210',
};

test.after.always('Destroy db client', () => {
	setTimeout(() => errorSaver.client.destroy(), 5000);
});

test('SaveError() should not throw creating instances', t => {
	t.notThrows(() => SaveError(), 'SaveError() instance creation should not throw');
});

test('saveError() should throw without "errors" param', t => {
	t.throws(() => errorSaver.saveError(), /error(.+required)?/i, `saveError() should throw "errors" is required`);
});

test('saveError(errors, table) should throw for invalid "errors" param i.e. if not (Array<Object> | Object)', t => {
	t.throws(() => errorSaver.saveError('some error', 'Archive Errors'), /error(.+required)?/i, `saveError('some error', 'Archive Errors') should throw valid "errors" is required`);

	t.throws(() => errorSaver.saveError(123456, 'Archive Errors'), /error(.+required)?/i, `saveError(123456, 'Archive Errors') should throw valid "errors" is required`);
});

test.skip('saveError(([] | {}), table) should not throw with valid "errors" param (Array<Object> | Object)', t => {
	t.notThrows(() => errorSaver.saveError([errorObj, errorObj], 'Archive Errors'), `saveError([errorObj, errorObj], 'Archive Errors') should not throw`);

	// t.notThrows(() => errorSaver.saveMessage(errorObj, 'Archive Errors'), `saveError(errorObj, 'Archive Errors') should not throw`);
});
