import appRoot from 'app-root-path';
import knex from 'knex';
import debug from 'debug';
import stampit from 'stampit';
import _ from 'lodash/fp';

const {KnexConfig} = require(`${appRoot}/config/db-config`);

/* Debug Logs */
const logDb = debug('gitter-archive:db');
const logDbVerbose = debug('gitter-archive:db');

const DbClientFactory = stampit({
	props: {
		client: null,
		roomsTable: 'Rooms',
		messagesTable: 'Messages',
		errorsTable: 'Archive Errors',
		usersTable: 'Users',
	},
	
	init(knexConfig, {instance}) {
		if (_.isNil(knexConfig)) {
			throw new Error('knex config object is required');
		}

		instance.client = knex(knexConfig);
	},
});

logDb('DB Module');

export {
	KnexConfig,
	DbClientFactory,
	logDb,
	logDbVerbose,
};
