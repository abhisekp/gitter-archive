import _ from 'lodash/fp';
import stampit from 'stampit';

const Rooms = stampit({
	methods: {
		getAllRooms({isArchive, isDeprecate, isArchiveComplete} = {}) {
			const whereQuery = {};
			/* eslint-disable camelcase */
			if (_.isBoolean(isArchive)) {
				whereQuery.is_archive = isArchive;
			}

			if (_.isBoolean(isDeprecate)) {
				whereQuery.is_deprecate = isDeprecate;
			}

			if (_.isBoolean(isArchiveComplete)) {
				whereQuery.is_archive_complete = isArchiveComplete;
			}
			/* eslint-enable camelcase */

			return this.client(this.roomsTable)
			.select()
			.where(whereQuery)
			.orderBy('user_count', 'asc')
			.then(_.map(::this.parseDbObjToRoom))
		}
	}
})

export {
	Rooms,
};
