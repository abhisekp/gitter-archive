import _ from 'lodash/fp';
import promiseRetry from 'promise-retry';
import stampit from 'stampit';

const SaveMessage = stampit({
	methods: {
		get saveMessage() {
			return this.saveMessages;
		},

		saveMessages(messages, table) {
			if (!this.parseMessageObjectToDb) {
				throw new Error('Compose "DbUtil" to save messages');
			}

			if (_.isNil(messages) || (!_.isArray(messages) && !_.isObject(messages))) {
				throw new Error('Valid "message(s)" is required (string OR object with text property)');
			}

			const _table = table || this.client.messagesTable;
			const _messages = [].concat(messages);

			_.each(message => {
				message.roomId = this.roomId;
				message.roomUri = this.roomUri;
			}, _messages);

			const dbMessages = _.map(::this.parseMessageObjectToDb, _messages);

			const taskFn = () => this.client.client.batchInsert(_table, dbMessages)
				.returning(['id', 'room_uri', 'room_id']);

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		},
	},
});

export {
	SaveMessage,
};
