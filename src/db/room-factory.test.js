import test from 'ava';
import appRoot from 'app-root-path';
import {RoomFactory} from './room-factory';
import {DbClientFactory} from './db-client-factory';

const {KnexConfig} = require(`${appRoot}/config/db-config`);
const dbClient = DbClientFactory(KnexConfig());

test('RoomFactory() instance creation throws without "client" options param', t => {
	t.throws(() => RoomFactory({roomId: '16516516651'}), /client\s*(.+required)?/i, 'RoomFactory() instance creation should throw without all required options params ({roomId, client})');
});

test('RoomFactory() instance creation throws without "roomId" options param', t => {
	t.throws(() => RoomFactory({client: dbClient}), /room\s*id(.+required)?/i, 'RoomFactory() instance creation should throw without all required options ({roomId, client})');
});

test('RoomFactory({roomId, client}) instance creation should not throw provided with required options params', t => {
	t.notThrows(() => RoomFactory({roomId: '1234567890', client: dbClient}), 'RoomFactory() instance creation should not throw with proper options ({roomId, client}) param');
});

test('markComplete() must be a function', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient});

	t.is(typeof roomInfo.markComplete, 'function', 'RoomFactory() must have a "markComplete" method');
});

test('getNextSkip() must be a function', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient});

	t.is(typeof roomInfo.getNextSkip, 'function', 'RoomFactory() must have a "getNextSkip" method');
});

test('RoomFactory() instance should contain rest params', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient, skip: 2, uri: '564984'});

	t.is(roomInfo.uri, '564984', 'RoomFactory() should contain rest params');
});

test('getEarliestMessage() must be a function', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient});

	t.is(typeof roomInfo.getEarliestMessage, 'function', 'RoomFactory() must have a "getLatestMessage" method');
});

test('saveMessageToDb() must be a function', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient});

	t.is(typeof roomInfo.saveMessageToDb, 'function', 'RoomFactory() must have a "saveMessageToDb" method');
});

test('saveErrorToDb() must be a function', t => {
	const roomInfo = RoomFactory({roomId: '1234567890', client: dbClient});

	t.is(typeof roomInfo.saveErrorToDb, 'function', 'RoomFactory() must have a "saveErrorToDb" method');
});
