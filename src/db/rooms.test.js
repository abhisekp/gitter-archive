import test from 'ava';
import _ from 'lodash/fp';
import appRoot from 'app-root-path';
import {Rooms} from './rooms';
import {DbClientFactory} from './db-client-factory';
import {DbUtil} from './db-util';

const {currDbEnv} = require(`${appRoot}/config/db-config`);

const knexConfig = require(`${appRoot}/knexfile`)[currDbEnv];

const client = DbClientFactory.compose(Rooms, DbUtil)(knexConfig)

test('Rooms instance must have a "getAllRooms" method', t => {
	const rooms = Rooms();
	const isGetAllRoomsExists = _.isFunction(rooms.getAllRooms);

	t.true(isGetAllRoomsExists, '"getAllRooms" method must exist in rooms instance');
});

test('getAllRooms() must return an array list of rooms', async t => {
	const roomsList = await client.getAllRooms({isArchive: true});
	const isRoomsListArray = _.isArray(roomsList);

	t.true(isRoomsListArray, 'client.getAllRooms() must return an array in promise');
});