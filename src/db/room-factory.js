// @flow

import stampit from 'stampit';
import _ from 'lodash/fp';
import promiseRetry from 'promise-retry';
import debug from 'debug';

/* Debug Logs */
const logRoomFactory = debug('gitter-archive:room-factory');
const logRoomFactoryVerbose = debug('gitter-archive:room-factory:verbose');

const RoomFactory = stampit({
	props: {
		roomId: null,
		roomUri: '',
		workerCount: 0,
		client: null,
		skip: 0,
		latestRequestSet: 0,
		resolvedSkip: 0,
	},

	statics: {
		SKIP_MAX: 5000,
		LIMIT_MAX: 100,
	},

	methods: {
		getNextSkip() {
			// @TODO
			return RoomFactory.SKIP_MAX - this.resolvedSkip;
		},

		getEarliestMessage() {
			if (!_.isFunction(this.parseDbObjectToMessage)) {
				throw new Error('Compose "DbUtil" to get latest messages');
			}

			if (!_.isString(this.client.messagesTable)) {
				throw new Error('"messagesTable" property must exist (string)');
			}

			// sort by date and get the latest
			const taskFn = () => this.client.client(this.client.messagesTable).first()
				.orderBy('sent_at', 'asc')
				.where({
					/* eslint-disable camelcase */
					room_id: this.roomId,
					room_uri: this.roomUri,
					/* eslint-enable camelcase */
				}).then(::this.parseDbObjectToMessage);

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		},

		getRoomInfo() {
			if (!_.isFunction(this.parseDbObjectToMessage)) {
				throw new Error('Compose "DbUtil" to get latest messages');
			}

			if (!_.isString(this.roomTable)) {
				throw new Error('"roomTable" property must exist (string)');
			}

			const taskFn = () => this.client(this.roomTable).first()
				.where({
					/* eslint-disable camelcase */
					room_id: this.roomId,
					room_uri: this.roomUri,
					/* eslint-enable camelcase */
				}).then(::this.parseDbObjectToMessage);

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		},

		markComplete(...args) {
			if (!_.isFunction(this.parseDbObjectToMessage)) {
				throw new Error('Compose "DbUtil" to get latest messages');
			}

			if (!_.isString(this.roomTable)) {
				throw new Error('"roomTable" property must exist (string)');
			}

			const dbRequest = () => this.client(this.roomTable)
				.update({
					/* eslint-disable camelcase */
					is_archive_complete: true,
					/* eslint-enable camelcase */
				}, '*')
				.where({
					/* eslint-disable camelcase */
					room_id: this.roomId,
					room_uri: this.roomUri,
					/* eslint-enable camelcase */
				}).then(_.flow(_.first, ::this.parseDbObjectToMessage));

			// update room table with completed
			const taskFn = () => new Promise(resolve => {
				if (this.workerCount === 0) {
					logRoomFactory('Marking the room "%o" as complete...', `${this.roomId} - ${this.roomUri}`);
					resolve(dbRequest());
				} else {
					logRoomFactory('There are still %o workers working ', this.workerCount);
					setTimeout(() => this.markComplete(...args), 5e3 /* 5 secs */);
				}
			});

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		},

		saveMessagesToDb(messages) {
			return this.saveMessages(messages);
		},

		get saveMessageToDb() {
			return this.saveMessagesToDb;
		},

		saveErrorToDb(error) {
			return this.saveError(error);
		},
	},

	init({roomId, roomUri, client, ...restparams} = {}, {instance, args}) {
		if (_.isNil(client)) {
			throw new Error('"client" is required');
		}

		if (!_.isString(roomId || args[0])) {
			throw new Error('"roomId" is required');
		}

		Object.assign(instance, restparams);
		instance.roomId = roomId || args[0];
		instance.roomUri = roomUri || args[1];
		instance.client = client || args[2];
	}
});

export {
	RoomFactory,
	logRoomFactory,
	logRoomFactoryVerbose,
};
