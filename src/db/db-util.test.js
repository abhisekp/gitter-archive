import test from 'ava';
import {
	parseMessageObjectToDb,
	DbUtil,
	parseDbObjectToMessage,
	parseErrorObjectToDb,
	parseDbObjectToError,
	parseRoomObjToDb,
	parseDbObjToRoom,
} from './db-util';

const messageObj = {
	id: '1234567890',
	text: 'some text',
	html: 'some text',
	sent: '2016-07-06T19:42:34.252Z',
	fromUser: {
		id: '9876543210',
		username: 'abhisekp',
	},
	groupId: undefined,
	roomId: '456987123',
	roomUri: 'some room',
};

/* eslint-disable camelcase */
const dbObj = {
	message_id: '1234567890',
	text: 'some text',
	html: 'some text',
	sent_at: '2016-07-06T19:42:34.252Z',
	from_userid: '9876543210',
	from_username: 'abhisekp',
	group_id: undefined,
	room_id: '456987123',
	room_uri: 'some room',
};
/* eslint-enable camelcase */

const errorObj = {
	skip: 0,
	beforeId: '1234567890',
	roomId: '9876543210',
};

/* eslint-disable camelcase */
const errorDbObj = {
	skip: 0,
	before_id: '1234567890',
	room_id: '9876543210',
};
/* eslint-enable camelcase */

const roomObj = {
	id: '1234567890',
	uri: 'FreeCodeCamp/hikes',
	userCount: 205,
	groupId: '498498498477',
	isArchive: true,
	isDeprecate: true,
	isArchiveComplete: false,
}

/* eslint-disable camelcase */
const roomDbObj = {
	room_id: '1234567890',
	room_uri: 'FreeCodeCamp/hikes',
	user_count: 205,
	group_id: '498498498477',
	is_archive: true,
	is_deprecate: true,
	is_archive_complete: false,
}
/* eslint-enable camelcase */

test('parseMessageObjectToDb() must be a function in dbUtil', t => {
	const util = DbUtil();

	t.is(typeof util.parseMessageObjectToDb, 'function', 'parseMessageObjectToDb() must be a function');
});

test('parseDbObjectToMessage() must be a function in dbUtil', t => {
	const util = DbUtil();

	t.is(typeof util.parseDbObjectToMessage, 'function', 'parseDbObjectToMessage() must be a function');
});

test('parseErrorObjectToDb() must be a function in dbUtil', t => {
	const util = DbUtil();

	t.is(typeof util.parseErrorObjectToDb, 'function', 'parseErrorObjectToDb() must be a function');
});

test('parseDbObjectToError() must be a function in dbUtil', t => {
	const util = DbUtil();

	t.is(typeof util.parseDbObjectToError, 'function', 'parseDbObjectToError() must be a function');
});

test('parse message object to db object', t => {
	const actualDbObj = parseMessageObjectToDb(messageObj);

	t.deepEqual(actualDbObj, dbObj, 'parseMessageObjectToDb(messageObj) should convert to db object');
});

test('parseDbObjectToMessage() should parse db object to message object', t => {
	const actualMessageObj = parseDbObjectToMessage(dbObj);

	t.deepEqual(actualMessageObj, messageObj, 'parseMessageObjectToDb(messageObj) should convert to db object');
});

test('parseErrorObjectToDb() should parse error object to db object', t => {
	const actualDbErrorObj = parseErrorObjectToDb(errorObj);

	t.deepEqual(actualDbErrorObj, errorDbObj, 'parseErrorObjectToDb(errorObj) should convert to db error object');
});

test('parseDbObjectToError() should parse error object to db object', t => {
	const actualErrorObj = parseDbObjectToError(errorDbObj);

	t.deepEqual(actualErrorObj, errorObj, 'parseErrorObjectToDb(errorObj) should convert to db error object');
});

test('parseRoomObjToDb() should parse room object to db object', t => {
	const actualRoomDbObj = parseRoomObjToDb(roomObj);

	t.deepEqual(actualRoomDbObj, roomDbObj, 'parseRoomObjToDb(roomDbObj) should convert to db room object');
})

test('parseDbObjToRoom() should parse room object to db object', t => {
	const actualRoomObj = parseDbObjToRoom(roomDbObj);

	t.deepEqual(actualRoomObj, roomObj, 'parseDbObjToRoom(roomDbObj) should convert to db room object');
})