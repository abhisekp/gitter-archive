// @flow

import _ from 'lodash/fp';
import promiseRetry from 'promise-retry';
import stampit from 'stampit';
import debug from 'debug';

/* Debug Logs */
const logSaveErrorDb = debug('gitter-archive:save-error-db');
const logSaveErrorDbVerbose = debug('gitter-archive:save-error-db:verbose');

const SaveError = stampit({
	methods: {
		saveError(errors, table) {
			if (!this.parseErrorObjectToDb) {
				throw new Error('Compose "DbUtil" to save error');
			}

			logSaveErrorDb('Errors:\n%o', errors);
			if (_.isNil(errors) || (!_.isArray(errors) && !_.isObject(errors))) {
				throw new Error('Valid "message(s)" is required (string OR object with text property)');
			}

			const _table = table || this.errorsTable;
			const _errors = [].concat(errors);

			_.each(error => {
				message.roomId = this.roomId;
				message.roomUri = this.roomUri;
			}, _errors);

			logSaveErrorDb('Table Name:\n%o', _table);

			const dbErrors = _.map(::this.parseErrorObjectToDb, _errors);
			const taskFn = () => this.client.client.batchInsert(_table, dbErrors)
				.returning(['skip', 'room_id', 'before_id']);

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		}
	}
});

export {
	SaveError,
	logSaveErrorDb,
	logSaveErrorDbVerbose,
};
