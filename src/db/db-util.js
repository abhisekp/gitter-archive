// @flow

import appRoot from 'app-root-path';
import stampit from 'stampit';
import _ from 'lodash/fp';

const {srcPath} = require(`${appRoot}/config/app-config`);
const {pickDeepAll} = require(`${appRoot}/${srcPath}/util`);

const parseDbObjectToMessage = dbMessageObj => {
	const _messageObj = pickDeepAll(['text', 'html', {
		id: 'message_id',
		groupId: 'group_id',
		sent: 'sent_at',
		roomId: 'room_id',
		roomUri: 'room_uri',
	}], dbMessageObj);

	const messageObj = _.assign({
		fromUser: {
			id: _.get('from_userid', dbMessageObj),
			username: _.get('from_username', dbMessageObj),
		},
	}, _messageObj);

	return messageObj;
};

const parseMessageObjectToDb = messageObj => {
	/* eslint-disable camelcase */
	const dbMessageObj = pickDeepAll(['text', 'html', {
		message_id: 'id',
		group_id: 'groupId',
		from_userid: 'fromUser.id',
		from_username: 'fromUser.username',
		sent_at: 'sent',
		room_id: 'roomId',
		room_uri: 'roomUri',
	}], messageObj);
	/* eslint-enable camelcase */

	return dbMessageObj;
};

const parseErrorObjectToDb = errorObj => {
	/* eslint-disable camelcase */
	const dbErrorObj = pickDeepAll(['skip', {
		before_id: 'beforeId',
		room_id: 'roomId',
	}], errorObj);
	/* eslint-enable camelcase */

	return dbErrorObj;
};

const parseDbObjectToError = errorDbObj => {
	const errorObj = pickDeepAll(['skip', {
		beforeId: 'before_id',
		roomId: 'room_id',
	}], errorDbObj);

	return errorObj;
};

const parseRoomObjToDb = roomObj => {
	/* eslint-disable camelcase */
	const dbObj = pickDeepAll([{
		room_id: 'id',
		room_uri: 'uri',
		user_count: 'userCount',
		group_id: 'groupId',
		is_archive: 'isArchive',
		is_deprecate: 'isDeprecate',
		is_archive_complete: 'isArchiveComplete',
	}], roomObj);
	/* eslint-enable camelcase */

	return dbObj;
};

const parseDbObjToRoom = dbObj => {
	const roomObj = pickDeepAll([{
		id: 'room_id',
		uri: 'room_uri',
		userCount: 'user_count',
		groupId: 'group_id',
		isArchive: 'is_archive',
		isDeprecate: 'is_deprecate',
		isArchiveComplete: 'is_archive_complete',
	}], dbObj);

	return roomObj;
};

const DbUtil = stampit({
	methods: {
		parseDbObjectToMessage,
		parseMessageObjectToDb,
		parseErrorObjectToDb,
		parseDbObjectToError,
		parseDbObjToRoom,
		parseRoomObjToDb,
	},
});

export {
	DbUtil,
	parseDbObjectToMessage,
	parseMessageObjectToDb,
	parseErrorObjectToDb,
	parseDbObjectToError,
	parseDbObjToRoom,
	parseRoomObjToDb,
};
