import test from 'ava';
import appRoot from 'app-root-path';
import {SaveMessage} from './save-message-db';
import {DbClientFactory} from './db-client-factory';
import {DbUtil} from './db-util';

const {currDbEnv} = require(`${appRoot}/config/db-config`);

const knexConfig = require(`${appRoot}/knexfile`)[currDbEnv];

const messageSaver = DbClientFactory.compose(DbUtil, SaveMessage).create(knexConfig);

const messageObj = {
	get id() {
		return Date.now();
	},
	text: 'some text',
	html: 'some text',
	sent: '2016-07-06T19:42:34.252Z',
	fromUser: {
		id: '9876543210',
		username: 'abhisekp',
	},
	groupId: undefined,
	roomId: '456987123',
	roomUri: 'some room',
};

test.after.always('Destroy db client', () => {
	setTimeout(() => messageSaver.client.destroy(), 5000);
});

test('SaveMessage() should not throw creating instances', t => {
	t.notThrows(() => SaveMessage(), 'SaveMessage() instance creation should not throw');
});

test('saveMessage is an alias of saveMessages', t => {
	t.is(messageSaver.saveMessage, messageSaver.saveMessages, '"saveMessage" and "saveMessages" should refer to the same function');
});

test('saveMessages(([] | {}), Messages) should not throw with valid "messages" param (Array<Object> | Object)', t => {
	t.notThrows(() => messageSaver.saveMessages([messageObj, messageObj], 'Messages'), `saveMessages([messageObj, messageObj], 'Messages') should not throw`);

	t.notThrows(() => messageSaver.saveMessage(messageObj, 'Messages'), `saveMessages(messageObj, 'Messages') should not throw`);
});

test('saveMessages() should throw without "messages" param', t => {
	t.throws(() => messageSaver.saveMessages(), /message(.+required)?/i, `saveMessages() should throw "messages" is required`);
});

test('saveMessages(messages, table) should throw for invalid "messages" param i.e. if not (Array<Object> | Object)', t => {
	t.throws(() => messageSaver.saveMessages('some message', 'Messages'), /message(.+required)?/i, `messageSaver.saveMessages('some message', 'Messages') should throw valid "messages" is required`);

	t.throws(() => messageSaver.saveMessages(123456, 'Messages'), /message(.+required)?/i, `saveMessages(123456, 'Messages') should throw valid "messages" is required`);
});
