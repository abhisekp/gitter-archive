import test from 'ava';
import _ from 'lodash/fp';
import appRoot from 'app-root-path';
import {DbClientFactory} from './db-client-factory';

const {currDbEnv} = require(`${appRoot}/config/db-config`);

const knexConfig = require(`${appRoot}/knexfile`)[currDbEnv];

test('DbClientFactory(knexConfig) with valid knexConfig should not throw', t => {
	t.notThrows(() => DbClientFactory(knexConfig), 'DbClientFactory(knexConfig) should not throw');
});

test('DbClientFactory(knexConfig) should have a client property', t => {
	const dbClient = DbClientFactory(knexConfig);

	const isClientPropExist = _.has('client', dbClient) && !_.isNil(dbClient.client);

	t.true(isClientPropExist, 'db client should have a "client" object property');
});

test.skip('DbClientFactory() without any param should throw', t => {
	t.throws(() => DbClientFactory(), /knex(.+required)?/i, 'DbClientFactory() must be given a "knexConfig" param');
});
