import _ from 'lodash/fp';
import debug from 'debug';
import {getChatMessages} from './gitter';
import {saveRoomMessages} from './save-db';
import {dbClient} from './db';
import {pickDeepAll} from './util';

/* Debug Logs */
const logFetchMessages = debug('gitter-archive:messages');
const logMessagesVerbose = debug('gitter-archive:messages:verbose');

/* eslint-disable camelcase */
const getAllRoomIdsToArchive = () => dbClient('Rooms').select('room_id', 'room_uri').where({is_archive: true})
	.map(({room_uri, room_id}) => ({roomId: room_id, roomUri: room_uri}));
/* eslint-enable camelcase */

let totalMessagesCount = 0;
/**
 *
 *
 * @param {any} {beforeId, messages, roomId}
 * @returns
 */
const saveDb = async ({beforeId, messages, roomId}) => {
	totalMessagesCount += messages.length;

	logFetchMessages(
		`Saved %o messages to "Room Message DB" from roomId %o before message id %o`,
		messages.length,
		roomId, beforeId,
	);

	// pick deep the selected message props for each message
	/* eslint-disable camelcase */
	const props = _.map(
		_.assign({room_id: roomId}, pickDeepAll(['text', 'html', {
			message_id: 'id',
			sent_at: 'sent',
			from_userid: 'fromUser.id',
			from_username: 'fromUser.username',
		}])),
		messages,
	);

	// save to actual database with table name "Messages"
	// and props as batch insert rows
	saveRoomMessages('Messages', props);
	/* eslint-enable camelcase */

	const nextBeforeId = _.flow(_.first, _.get('id'));

	return Promise.resolve({
		beforeId: nextBeforeId(messages),
		roomId,
		messages,
	});
};

/**
 *
 *
 * @param {any} {beforeId, error, roomId}
 */
const saveDbError = ({beforeId, error, roomId}) => {
	logFetchMessages(
		`Saved to "Error DB" from roomId %o before message id %o with error message %o`,
		roomId, beforeId,
		error.message,
	);

	/* eslint-disable camelcase */
	return dbClient('Archive Errors').insert({
		room_id: roomId,
		before_id: beforeId,
	});
	/* eslint-enable camelcase */
};

/**
 *
 *
 * @param {any} {beforeId, roomId}
 * @returns
 */
const fetchMessages = ({beforeId, roomId, roomUri}) => {
	return getChatMessages({beforeId, roomId})
		.then(_.thru(saveDb), _.tap(saveDbError))
		.then(_.tap(({roomId, beforeId}) => {
			if (_.isNil(beforeId)) {
				logFetchMessages(
					'Total Messages saved from roomId %o, roomUri %o is %o',
					roomId, roomUri, totalMessagesCount,
				);

				return;
			}

			fetchMessages({roomId, beforeId, roomUri});
		}));
};

// const fccHikeRoomId = '55ea1cee0fc9f982beafb358';
// logFetchMessages('Room Id is set to', fccHikeRoomId);
// fetchMessages({roomId: fccHikeRoomId});

logFetchMessages('Start Fetching Messages');
getAllRoomIdsToArchive().then(rooms => {
	_.forEach(({roomId, roomUri}) => {
		logFetchMessages('Room Id: %o and Uri: %o', roomId, roomUri);
		fetchMessages({roomId, roomUri});
	}, rooms);
});

export {
	fetchMessages,
	logFetchMessages,
	logMessagesVerbose,
};
