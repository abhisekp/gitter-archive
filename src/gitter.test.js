// @flow

import test from 'ava';
import _ from 'lodash/fp';
import {GitterClientFactory, RoomResources, GroupResources} from './gitter';

const getClient = GitterClientFactory.compose(RoomResources, GroupResources);
const client = getClient('1234567890');

test('instantiating composition of gitter client with other resource stamps should give an object', t => {
	t.is(typeof client, 'object', 'Instance of Composing GitterClientFactory with other resources must be an object');
});

test('composing gitter client with other resources must have their methods in the instance', t => {
	const isGetChatMessagesExist = _.isFunction(client.getChatMessages);
	const isGetAllRoomsExist = _.isFunction(client.getAllRooms);
	const isGetAllGroupsExist = _.isFunction(client.getAllGroups);
	const isRequestExist = _.isFunction(client.request);

	t.true(isGetChatMessagesExist, 'client must have getChatMessages() method');
	t.true(isGetAllRoomsExist, 'client must have getAllRoomsExist() method');
	t.true(isGetAllGroupsExist, 'client must have getAllGroupsExist() method');
	t.true(isRequestExist, 'client must have request() method');
});
