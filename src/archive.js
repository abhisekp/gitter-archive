import 'babel-polyfill';
import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {dbClient, gitterClients} from './core';
import {
	RoomFactory,
	DbUtil,
	SaveMessage,
	SaveError,
} from './db';
import cyclicNext from 'cyclic-next';

const Room = RoomFactory.compose(DbUtil, SaveMessage, SaveError);

const splitSmallLargeRooms = rooms => {
	return _.partition(room => room.userCount < 100, rooms);
}

const getRoomsInfos = _.map(
	({id: roomId, uri: roomUri, ...restparam}) =>
		Room({roomId, roomUri, client: dbClient, ...restparam})
);

const getAllChatMessages = ({skip = 0, beforeId, roomInfo: {roomId, roomUri}, roomInfo}) => {
	const messagePromises = [];
	const dbPromises = [];
	roomInfo.latestRequestSet += 1;
	console.log('Request Set #%o', roomInfo.latestRequestSet);

	for(let _skip = RoomFactory.SKIP_MAX; _skip >= skip; _skip -= RoomFactory.LIMIT_MAX) {
		roomInfo.workerCount += 1;
		// request gitter for chat messages
		const messagePromise = gitterClients[0].getChatMessages({
			beforeId, skip: _skip, roomId, roomUri, currRequestSet: roomInfo.latestRequestSet
		})
			.then(_.tap(({messages}) => {
				console.log('Saving to message db');
				const dbPromise = roomInfo.saveMessagesToDb(messages).catch(_.tap(::console.error));
				dbPromises.push(dbPromise);
			}), _.tap(({error, skip, beforeId, roomId, roomUri}) => {
				console.log('Saving to error db');
				const dbPromise = roomInfo.saveErrorToDb({error, skip, beforeId, roomId, roomUri}).catch(_.tap(::console.error));
				dbPromises.push(dbPromise);
			}))
			.then(({currRequestSet, skip, messages = []}) => {
				roomInfo.workerCount -= 1;
				
				if (_.isEmpty(messages)) {
					roomInfo.markComplete();
					return messages;
				}

				if (currRequestSet !== roomInfo.latestRequestSet && skip <= roomInfo.resolvedSkip) {
					return messages;
				}

				roomInfo.resolvedSkip = skip;
				const latestMessageId = _.flow(_.first, _.get('id'))(messages);
				console.log('Latest Message Id: %o', latestMessageId);

				return getAllChatMessages({
					skip: roomInfo.getNextSkip(),
					beforeId: latestMessageId,
					roomInfo,
				});
			})
		messagePromises.push(messagePromise);
	}
	return Promise.all(messagePromises);
}

const getAllIncompleteRoomsInfos = async () => {
	const rooms = await dbClient.getAllRooms({isArchive: true, isArchiveComplete: false});
	const roomsInfos = getRoomsInfos(rooms);
	return roomsInfos;
}

const run = async () => {
	const rooms = await getAllIncompleteRoomsInfos();

	const largestRoomInfo = _.last(rooms);

	const earliestMessage = await largestRoomInfo.getEarliestMessage();
	const earliestMessageId = _.get('id', earliestMessage);

	getAllChatMessages({
		roomId: largestRoomInfo.roomId,
		roomInfo: largestRoomInfo,
		roomUri: largestRoomInfo.roomUri,
		beforeId: earliestMessageId,
	});


// 	console.dir(largestRoomInfo, {colors: 1, depth: 0});

// 	const messagePromises = [];
// 	for(let skip = 0; skip < 5000; skip += 1) {
// 		const messagePromise = gitterClients[0].getChatMessages({...largestRoomInfo, skip});
// 		messagePromises.push(messagePromise);
// 	}

// 	const {messages} = await gitterClients[0].getChatMessages({...largestRoomInfo});
// 	const _message = _.first(messages);
// // 	console.dir(_message, {colors: 1, depth: 1});

// 	const savedMessages = await largestRoomInfo.saveMessageToDb(_message);
// 	console.dir(savedMessages, {colors: 1, depth: 1});
}

run();

export {};
