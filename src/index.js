import {isProd} from '@/config/app-config';

if (isProd) {
	require('babel-polyfill');
}

export * from './archive';
