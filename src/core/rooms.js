import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {dbClient} from './db-client';

/* eslint-disable camelcase */
const getAllArchiveRooms = () => dbClient.client(dbClient.roomsTable)
	.select()
	.where({is_archive: true, is_archive_complete: false})
	.orderBy('user_count', 'asc')
	.map(({room_uri, room_id, user_count}) => ({roomId: room_id, roomUri: room_uri, userCount: user_count}));
/* eslint-enable camelcase */

export {
	getAllArchiveRooms,
};
