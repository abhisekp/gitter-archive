import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {
	DbClientFactory,
	DbUtil,
	Rooms,
	KnexConfig as DefaultKnexConfig
} from '../db';

const {currDbEnv} = require(`${appRoot}/config/db-config`);

const knexConfig = require(`${appRoot}/knexfile`)[currDbEnv];

const createDbClient = DbClientFactory.compose(DbUtil, Rooms);

const dbClient = createDbClient(knexConfig);

export {
	createDbClient,
	dbClient,
};
