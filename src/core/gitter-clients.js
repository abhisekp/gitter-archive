import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {GitterClientFactory, RoomResources, GroupResources} from '../gitter';

const {gitterTokens} = require(`${appRoot}/config/app-config`);

// create gitter clients using token
const createGitterClient = GitterClientFactory.compose(RoomResources, GroupResources);

const getAllGitterClients = _.map(createGitterClient);

const gitterClients = getAllGitterClients(gitterTokens);

export {
	getAllGitterClients,
	gitterClients,
};
