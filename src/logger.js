import path from 'path';
import appRoot from 'app-root-path';
import debug from 'debug';
import winston from 'winston';
import _ from 'lodash/fp';
import {appConfig} from '@/config/app-config';

/* Debug Logs */
const logDebug = debug('gitter-archive:logger');
const logDebugVerbose = debug('gitter-archive:logger:verbose');

logDebugVerbose('Gitter Achive Config\n%o', appConfig);
const {isProd, env} = appConfig;

// setup winston log level
winston.level = env.LOG_LEVEL || 'debug';
if (isProd) {
	winston.level = env.LOG_LEVEL || 'error';
}
logDebug('winston.level: %o', winston.level);

// winston log files
const logFilePath = env.LOG_FILE || path.resolve(`${appRoot}/all-logs.log`);
logDebug('logFilePath: %o', logFilePath);

const exceptionFilePath = env.EXCEPTION_FILE || path.resolve(`${appRoot}/exceptions.log`);
logDebug('exceptionFilePath: %o', exceptionFilePath);

// winston loggers
const logger = new (winston.Logger)({
	transports: [
		/* new winston.transports.Console({
			color: true,
			timestamp: true
		}),*/
		new winston.transports.File({
			// json: false,
			filename: logFilePath
		})
	],
	exceptionHandlers: [
		new winston.transports.File({
			// json: false,
			filename: exceptionFilePath
		})
	]
});
logDebugVerbose('winston logger transports %o', logger.transports);
logDebugVerbose('winston logger exception handlers %o', logger.exceptionHandlers);

export {
	logger,
	logger as default,
};
