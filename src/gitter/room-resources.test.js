import test from 'ava';
import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {RoomResources} from './room-resources';
import {GitterClientFactory} from './gitter-client-factory';

const {gitterTokens} = require(`${appRoot}/config/app-config`);
const token = gitterTokens[0];
const client = GitterClientFactory.compose(RoomResources)(token);

const fccRoomId = '546fd572db8155e6700d6eaf'; /* FreeCodeCamp/FreeCodeCamp */

test('getChatMessages() should throw as roomId is required', t => {
	t.throws(() => client.getChatMessages(), /room\s*id.+required/i, 'should throw roomId is required');
});

test('getChatMessages({roomId}) should not throw', t => {
	t.notThrows(() => client.getChatMessages({roomId: fccRoomId}), 'getChatMessages({roomId}) should not throw using valid roomId');
});

test('getChatMessages({roomId}) should respond with an array', async t => {
	const {messages} = await client.getChatMessages({roomId: fccRoomId});

	const isMessagesArray = _.isArray(messages);

	t.true(isMessagesArray, '`messages` response must be an array');
});
