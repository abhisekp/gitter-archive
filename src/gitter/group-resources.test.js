import debug from 'debug';
import _ from 'lodash/fp';
import test from 'ava';
import appRoot from 'app-root-path';
import {GroupResources} from './group-resources';
import {GitterClientFactory} from './gitter-client-factory';

const {gitterTokens} = require(`${appRoot}/config/app-config`);
const token = gitterTokens[0];
const client = GitterClientFactory.compose(GroupResources)(token);

/* Debug Logs */
const logRooms = debug('gitter-archive:rooms');
const logRoomsVerbose = debug('gitter-archive:rooms:verbose');

const fccGroupId = '57542cf4c43b8c6019778297'; /* FreeCodeCamp */

test('getAllRooms({groupId}) should not throw', t => {
	t.notThrows(() => client.getAllRooms({groupId: fccGroupId}), 'getAllRooms({groupId}) method should not throw with valid groupId');
});

test('getAllRooms({groupId}) should respond with an array', async t => {
	const {rooms} = await client.getAllRooms({groupId: fccGroupId});

	_.forEach(room => {
		const roomInfo = _.pickAll(['uri', 'id'], room);
		logRoomsVerbose('%o', roomInfo);
	}, rooms);

	const isRoomArray = _.isArray(rooms);

	t.true(isRoomArray, 'The "groups" response must be an array');
});

test('GroupResources() instance should have "getAllRooms" method', t => {
	const groupResources = GroupResources();

	t.is(typeof groupResources.getAllRooms, 'function', 'GroupResources() instance should have "getAllRooms" method');
});

export {
	logRooms,
	logRoomsVerbose,
};
