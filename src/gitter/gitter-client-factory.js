// @flow

import debug from 'debug';
import RateLimiterAPI from 'rate-limiter-api';
import request from 'superagent';
import stampit from 'stampit';
import promiseRetry from 'promise-retry';
import _ from 'lodash/fp';
// import {dedent} from 'dentist';
// import {logger} from './logger';

/* Debug Logs */
const logGitter = debug('gitter-archive:gitter');
const logGitterVerbose = debug('gitter-archive:gitter:verbose');

const GitterClientFactory = stampit({
	statics: {
		clientList: new Map()
	},

	props: {
		token: '',
		rateLimiter: null,
	},

	init(token, {instance}) {
		if (!_.isString(token) || !token.length) {
			throw new Error('TOKEN is required.');
		}

		if (GitterClientFactory.clientList.has(token)) {
			return GitterClientFactory.clientList.get(token);
		}

		instance.token = token;
		instance.rateLimiter = RateLimiterAPI();

		GitterClientFactory.clientList.set(token, instance);
	},

	methods: {
		request({
			url, /* url endpoint */
			query = {}, /* query param */
			headers = {}, /* extra headers */
			method = 'GET', /* method type */
		} = {}) {
			if (_.isNil(url)) {
				throw new Error('url is required');
			}

			const _headers = _.defaults({
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${this.token}`,
			}, headers);
			logGitter('Headers:\n%o', _headers);

			const taskFn = retry => this.rateLimiter.limit(responseHandler => {
				request[_.toLower(method)](url).set(_headers).query(query)
					.then(response => {
						const limits = {
							rateLimit: response.header['x-ratelimit-limit'],
							rateRemaining: response.header['x-ratelimit-remaining'],
							rateReset: response.header['x-ratelimit-reset'],
						};
						logGitter(`Rate Limits:\n%o`, limits);

						// update rate limits
						this.rateLimiter.updateRateLimits(limits);

						logGitterVerbose('Response Body:\n%o', response);
						responseHandler(null, response);
					})
					.catch(err => {
						const response = err.response;

						// @FIXME use winston
						// console.dir(err.response.clientError, {colors: 1, depth: 0});

						const isClientError = response.clientError;
						logGitter('Is Client Error? %o', isClientError);

						responseHandler(err);
					});
			}).catch(err => {
				if (_.get('response.clientError', err)) {
					throw err;
				}

				return retry(err);
			});

			return promiseRetry({
				retries: 3,
				maxTimeout: 60 * 60 * 1000, /* 1 hour */
			}, taskFn);
		}
	}
});

export {
	logGitter,
	logGitterVerbose,
	GitterClientFactory,
};
