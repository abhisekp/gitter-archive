import debug from 'debug';
import test from 'ava';
import appRoot from 'app-root-path';
import _ from 'lodash/fp';
import {GitterClientFactory} from './gitter-client-factory';

const {gitterTokens} = require(`${appRoot}/config/app-config`);

/* Debug Logs */
const logGitterClientFactoryTest = debug('gitter-archive:test:client');

const GITTER_TOKEN = gitterTokens[0];

test('gitter client without token throws on client creation', t => {
	t.throws(() => GitterClientFactory(), /token(.+required)?/i, 'GitterClient() should throw an error to provide TOKEN');
	t.throws(() => GitterClientFactory(''), /token(.+required)?/i, `GitterClient('') should throw an error to provide TOKEN.`);
});

test('gitter client with invalid token throws on `.request()` with Unauthorized error', t => {
	t.notThrows(() => GitterClientFactory('1234567890'), `GitterClient('1234567890') should not throw error although TOKEN is invalid.`);

	const gitterClient = GitterClientFactory('1234567890');
	logGitterClientFactoryTest('%o', gitterClient);

	const response = gitterClient.request({
		url: 'https://api.gitter.im/v1/user/me'
	});

	t.throws(response, /Unauthorized/i, `Request to GitterClient('1234567890') should throw "Unauthorized" error`);
});

test('gitter client with valid token should not throw on client creation', t => {
	t.notThrows(() => GitterClientFactory(GITTER_TOKEN), `GitterClient(GITTER_TOKEN) should not throw error although TOKEN is invalid.`);
});

test('gitter client with valid token should respond with an object', async t => {
	const gitterClient = GitterClientFactory(GITTER_TOKEN);
	logGitterClientFactoryTest('%o', gitterClient);

	const {body: response} = await gitterClient.request({
		url: 'https://api.gitter.im/v1/user/me'
	});

	t.is(typeof response, 'object', 'Response should be an object.');
});

test('gitter client with valid token should throw without any options in `request()` method', t => {
	const gitterClient = GitterClientFactory(GITTER_TOKEN);
	logGitterClientFactoryTest('%o', gitterClient);

	t.throws(() => gitterClient.request(), /url(.+required)?/i, 'request() without "url" should throw');
});

test('Get the same client for a given token', t => {
	const TOKEN = '9876543210';

	const client1 = GitterClientFactory(TOKEN);
	logGitterClientFactoryTest(`client1(${TOKEN}): %o`, client1);

	const client2 = GitterClientFactory(TOKEN);
	logGitterClientFactoryTest(`client1(${TOKEN}): %o`, client2);

	t.deepEqual(client1, client2, 'Client 1 must be equal to client 2');
});

test('GitterClientFactory must have a "clientList" Map object', t => {
	const clientList = GitterClientFactory.clientList;
	const isClientListIterable = _.isObject(clientList) ? _.isFunction(clientList[Symbol.iterator]) : false;
	const isClientListMap = _.isMap(clientList);

	t.true(isClientListIterable, 'clientList must be an iterable object');
	t.true(isClientListMap, 'clientList must be a "Map" object');
});
