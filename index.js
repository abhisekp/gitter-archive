require('babel-core/register');
const appRoot = require('app-root-path');

const {srcPath} = require(`${appRoot}/config/app-config`);
require(`${appRoot}/${srcPath}`);
