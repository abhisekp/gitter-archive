/* eslint-disable xo/filename-case */

const roomTableName = 'Rooms';
const errorsTableName = 'Archive Errors';
const messagesTableName = 'Messages';
const usersTableName = 'Users';

exports.up = function (knex, Promise) {
	return Promise.all([
		// Room List Table
		knex.schema.createTable(roomTableName, table => {
			table.increments('id').notNullable().unique();
			table.dropPrimary();
			table.string('room_id', 30).notNullable(); // provide
			table.string('room_uri', 100).notNullable(); // provide
			table.integer('user_count').unsigned(); // provide
			table.string('group_id', 30); // provide
			table.boolean('is_archive').notNullable().defaultsTo(false); // provide
			table.boolean('is_deprecate').notNullable().defaultsTo(false); // provide
			table.boolean('is_archive_complete').notNullable().defaultsTo(false);
			table.timestamps(true, true);
			table.primary(['room_id', 'room_uri'], 'room_id_uri_pkey');
		}),

		// Errors Table
		knex.schema.createTable(errorsTableName, table => {
			table.increments('id').primary('message_error');
			table.string('room_id', 30).notNullable(); // .references('room_id').inTable(roomTableName);
			table.string('before_id', 30).notNullable();
			table.string('error_message', 255);
			table.integer('skip').unsigned().defaultsTo(0).notNullable();
			table.timestamps(true, true);
		}),

		// Messages Table
		knex.schema.createTable(messagesTableName, table => {
			table.increments('id').notNullable().unique();
			table.dropPrimary();
			table.string('group_id', 30);
			table.string('room_id', 30).notNullable();
			table.string('room_uri', 100).notNullable();
			table.string('from_userid', 30);
			table.string('from_username', 255);
			table.string('message_id', 30).notNullable();
			table.string('text', 10000).notNullable();
			table.string('html', 20000).defaultsTo('');
			table.timestamp('sent_at').index('sent_at_idx');
			table.timestamps(true, true);
			table.primary(['room_id', 'message_id'], 'room_id_msg_id_pkey');
		}),

		// Users Table
		knex.schema.createTable(usersTableName, table => {
			table.increments('id').notNullable().unique();
			table.dropPrimary();
			table.string('userid', 30).notNullable();
			table.string('username', 255).notNullable();
			table.string('display_name', 255);
			table.string('company', 255);
			table.string('location', 255);
			table.string('email', 255);
			table.string('website', 255);
			table.string('profile', 255);
			table.timestamps(true, true);
			table.primary(['userid', 'username'], 'user_id_name_pkey');
		})
	]);
};

exports.down = function (knex, Promise) {
	return Promise.all([
		knex.schema.dropTableIfExists(roomTableName),
		knex.schema.dropTableIfExists(errorsTableName),
		knex.schema.dropTableIfExists(messagesTableName),
		knex.schema.dropTableIfExists(usersTableName)
	]);
};
