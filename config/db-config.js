const os = require('os');
const stampit = require('stampit');
const _ = require('lodash/fp');
const debug = require('debug');
const {env} = require('./app-config');

/* Debug Logs */
const logDbConfig = debug('gitter-archive:db:config');
const logDbConfigVerbose = debug('gitter-archive:db:config:verbose');

const currDbEnv = env.DB_ENV || env.NODE_ENV;

// @TODO prompt password should prompt for a password
// i.e. it shouldn't output anything to the stdout
// and only take inputs (stdin).
// Use a library (module) to do.
const promptPassword = _.constant('');

const currentUser = _.toLower(_.get('username', os.userInfo()));
logDbConfig('Current User: %o', currentUser);

const KnexConfig = stampit({
	deepProps: {
		client: 'pg',
		connection: {
			database: env.PGDATABASE || currentUser,
			user: env.PGUSER || currentUser,
			password: env.PGPASSWORD || promptPassword() || '',
			port: env.PGPORT || 5432,
			host: env.PGHOST || '127.0.0.1'
		},
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: 'knex_migrations'
		}
	}
});

debug('DB Config')('!! Module Loaded !!');

module.exports = {
	KnexConfig,
	DefaultKnexConfig: KnexConfig,
	logDbConfig,
	logDbConfigVerbose,
	currDbEnv,
};
